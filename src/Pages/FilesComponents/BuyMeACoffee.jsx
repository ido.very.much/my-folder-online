import { Component } from 'react';

export default class BuyMeACoffee extends Component {
    constructor(props) {
        super(props);

        const attributes = {
            "src": "https://cdnjs.buymeacoffee.com/1.0.0/button.prod.min.js",
            "data-name": "bmc-button",
            "data-slug": "folder.online",
            "data-color": "#FFDD00",
            "data-emoji": "🍕",
            "data-font": "Bree",
            "data-text": "Buy me a pizza",
            "data-outline-color": "#000000",
            "data-font-color": "#000000",
            "data-coffee-color": "#ffffff",
        }

        const script = document.createElement("script");

        for(const [key, value] of Object.entries(attributes)){
            script.setAttribute(key, value);

        }
        script.async = true

        //Call window on load to show the image
        script.onload = function () {
            const evt = document.createEvent('Event');
            evt.initEvent('DOMContentLoaded', false, false);
            window.dispatchEvent(evt);
        }

        this.script = script;

        this.state = {
            button: ''
        }
    }

    componentDidMount() {

        document.writeln = (text) => {
            this.setState({ button: <div style={{zoom: 0.6}} dangerouslySetInnerHTML={{__html: text}}></div>});
        }

        document.head.appendChild(this.script);
    }

    componentWillUnmount() {
        document.head.removeChild(this.script);
        document.body.removeChild(document.getElementById("bmc-wbtn"));
    }

    render() {
        return <div>{this.state.button}</div>
    }
}