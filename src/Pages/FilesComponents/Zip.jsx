import { Component } from 'react';
import xr from "../../files/xr";
import socketio from "socket.io-client";
import { v4 as uuidv4 } from 'uuid';
import { toast } from 'react-toastify';
import Collapsible from 'react-collapsible';
import Fade from './Fade';

import '../../files/index.sass'

class Zip extends Component {
    constructor(props) {
        super(props);

        this.state = {
            OnZip: [],
            Status: '📖'
        }

        this.ClearAllEnd = this.ClearAllEnd.bind(this);
        this.AddZip = this.AddZip.bind(this);
    }

    StartSocket(){
        return new Promise(res => {
            this.socket = socketio.connect(xr.basePath, {
                path: "/socket.io/",
                reconnectionAttempts: 10
            });
    
            this.socket.on('update-zip', this.UpdateInFiles.bind(this));
            this.socket.on('zip-end', this.OnZipEnd.bind(this));
            this.socket.on('send-error-warning', this.ErrorWarning.bind(this));
    
            this.socket.once('connect', res);
        });
    }

    SendCancel(id) {
        this.socket.emit('cancel-zip', id);
    }

    CancelZip(x, index) {
        if (x.progress !== 100 && !x.error) {
            this.SendCancel(x.id);
        }

        const copyArray = [...this.state.OnZip];

        copyArray.splice(index, 1);

        this.setState({
            OnZip: copyArray
        });
    }

    async AddZip(PathArray, startPath, name) {
        const copyArray = [...this.state.OnZip];

        const id = uuidv4();

        if(!this.socket){
            await this.StartSocket();
        }

        this.socket.emit('create-zip', { array: PathArray, id, startPath });

        copyArray.push({
            name: name + ".zip",
            id,
            zip: '0 Bits',
            scan: '0 Bits',
            progress: 0,
            error: false,
            PathArray,
            startPath
        });

        this.setState({
            OnZip: copyArray
        });
    }

    UpdateInFiles(data) {
        const id = data.id;
        delete data.id;

        const copyArray = [...this.state.OnZip];

        const index = copyArray.findIndex(x => x.id === id);

        if (index === -1) {
            this.CancelZip(copyArray[index], index);
        } else {
            copyArray[index] = Object.assign({}, { ...copyArray[index], ...data });

            this.setState({
                OnZip: copyArray
            });
        }
    }

    OnZipEnd(id) {
        const e = this.state.OnZip.find(x => x.id === id);
        if (e) {
            this.UpdateInFiles({ id, zip: e.scan, progress: 100 });
            const link = document.createElement('a');
            link.href = xr.basePath + `/getZip?id=${id}&name=${encodeURIComponent(e.name)}`;
            link.click();
            link.remove();
        }
    }

    ErrorWarning({ id, info, type }) {
        const zip = this.state.OnZip.find(x => x.id === id);
        if (zip === -1) {
            return;
        }

        if (type === "warning") {
            toast.warning(zip.name + " - " + info);
        } else {
            this.UpdateInFiles({ id, error: true });
            toast.error(zip.name + " - " + info);
        }
    }

    ClearAllEnd() {
        const copyArray = [];
        for (const i of this.state.OnZip) {
            if (i.progress !== 100) {
                copyArray.push(i);
            }
        }

        this.setState({
            OnZip: copyArray
        });
    }


    render() {
        return (<Fade show={this.state.OnZip.length} className="zip-box bg-white p-2 border">
            <div className="d-flex justify-content-between mb-2">
                <p className="h5">Zips</p>
                <button className="m-2 btn btn-outline-danger" onClick={this.ClearAllEnd}>Clear</button>
            </div>

            <Collapsible trigger={this.state.Status} open={true} onClose={() => this.setState({ Status: '📘' })} onOpen={() => this.setState({ Status: '📖' })}>
                <div className="all-zip">
                    {this.state.OnZip.map((x, index) => (
                        <div className="zip-toast border p-2 rounded mb-2" key={"zip_" + index}>
                            <div className="d-flex justify-content-between">
                                <p className="zip-title"><u>{x.error ? "Error - " + x.name : x.name}</u></p>
                                <div>
                                    <span className="m-2 close-zip" onClick={() => this.CancelZip(x, index)}>{x.progress === 100 || x.error ? 'X' : 'Cancel'}</span>
                                </div>
                            </div>
                            <div className="progress-data">{x.zip} / {x.scan}</div>
                            <Collapsible trigger="More info" contentOuterClassName="bg-light">
                                <b>folder</b>: {"Base folder" + x.startPath}<br />
                                <b>files and folders</b>
                                <ul>
                                    {x.PathArray.map(x => (<li key={x.startPath + x}>{x}</li>))}
                                </ul>
                            </Collapsible>
                            {x.progress != 100 ?
                                <div className="progress">
                                    <div className="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style={{ width: x.progress + "%" }}></div>
                                </div> : null
                            }
                        </div>
                    ))}</div>
            </Collapsible>
        </Fade>);
    }
}

export default Zip;