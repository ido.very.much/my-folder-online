import { Component } from 'react';

class Fade extends Component {
    constructor(props) {
        super(props);

        this.state = {
            type: 'fade hidden'
        }
    }

    componentDidUpdate(oldProps) {
        if(this.props.show != oldProps.show){
            if (this.props.show) {
                this.setState({ type: "fade show" });
            } else {
                this.setState({ type: "fade" });
                setTimeout(() => {
                    this.setState({ type: "fade hidden" });
                }, 1000 * 0.15);
            }
        }
    }

    render() {
        return (<div className={this.props.className + " " + this.state.type}>
            {this.props.children}
        </div>)
    }
}

export default Fade;