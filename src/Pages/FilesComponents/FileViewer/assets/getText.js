export default async function(path){
    const text = await fetch(path);
    return await text.text();
}