// Copyright (c) 2017 PlanGrid, Inc.

import React, { Component } from 'react';

import '../../styles/photo-viewer.scss';

export default class PhotoViewer extends Component {
  componentDidMount() {
    const { originalWidth, originalHeight } = this.props;
    const imageDimensions = this.getImageDimensions.call(this, originalWidth, originalHeight);

    this.props.texture.image.style.width = `100%`;
    this.props.texture.image.style.height = `100%`;
    this.props.texture.image.setAttribute('class', 'photo');
    document.getElementById('pg-photo-container').appendChild(this.props.texture.image);
  }

  getImageDimensions(originalWidth, originalHeight) {
    // Scale image to fit into viewer
    let imgHeight;
    let imgWidth;
    const { height: viewerHeight, width: viewerWidth } = this.props;

    if (originalHeight <= viewerHeight && originalWidth <= viewerWidth) {
      imgWidth = originalWidth;
      imgHeight = originalHeight;
    } else {
      const heightRatio = viewerHeight / originalHeight;
      const widthRatio = viewerWidth / originalWidth;
      if (heightRatio < widthRatio) {
        imgHeight = originalHeight * heightRatio;
        imgWidth = originalWidth * heightRatio;
      } else {
        imgHeight = originalHeight * widthRatio;
        imgWidth = originalWidth * widthRatio;
      }
    }

    return { height: imgHeight, width: imgWidth };
  }

  render() {
    return (
      <div className="photo-viewer-container" id="pg-photo-container" />
    );
  }
}
