import ReactMarkdown from 'react-markdown';
import getText from '../../assets/getText';
import {useState} from 'react';
import Loading from '../loading'

export default function({filePath}){
    const [data, setData] = useState('');

    getText(filePath).then(setData);

    return data ? <ReactMarkdown>{data}</ReactMarkdown>: <Loading/>
}