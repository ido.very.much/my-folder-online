import {Controlled as CodeMirror} from 'react-codemirror2'
import getText from '../../assets/getText';
import {useState} from 'react';
import Loading from '../loading';

import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/eclipse.css';
import '../../assets/codeMirror.sass';

export default function({filePath, mode}){
    const [data, setData] = useState('');

    import(`codemirror/mode/${mode}/${mode}.js`);

    getText(filePath).then(setData);

    return (data ? <CodeMirror
        value={data}
        options={{
          mode,
          theme: 'eclipse',
          lineNumbers: true,
          readOnly: true
        }}
      />: <Loading/>)
}