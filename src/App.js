import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import React, { Suspense, lazy, useState, useEffect } from 'react';
import BuyMeACoffee from './Pages/FilesComponents/BuyMeACoffee';
import "bootstrap";

import passManager from './files/passManager';

import 'bootstrap/dist/css/bootstrap.min.css';

import RingLoader from "react-spinners/RingLoader";

import { css } from "@emotion/core";

const Home = lazy(() => import('./Pages/Home'));
const Download = lazy(() => import('./Pages/Download'));
const Files = lazy(() => import('./Pages/Files'));
const Login = lazy(() => import('./Pages/Login'));
const FastLink = lazy(() => import('./Pages/FastLink'));

let NowActive = () => { };

function NavMenu({ name, link = name }) {
  const [active, setActive] = useState(window.location.pathname === "/" + link);

  if (active) {
    NowActive = setActive;
  }

  function IamActive() {
    NowActive(false);
    setActive(true);
    NowActive = setActive;
  }

  return (
    <li className="nav-item">
      <Link className={"nav-link" + (active ? ' active' : '')} onClick={IamActive} aria-current="page" to={link}>{name}</Link>
    </li>
  )
}

function App() {
  const [isLogin, setIsLogin] = useState(false);
  const [DarkMode, setDarkMode] = useState(localStorage.getItem("dark"));

  if (!location.pathname.startsWith('/FastLink/'))
    passManager.CheckStoredPassword().then(setIsLogin);

  passManager.onLoad = () => setIsLogin(true);

  function LogoutNow() {
    passManager.Logout();
    setIsLogin(false);
  }

  const override = css`
    display: block;
    margin: 0 auto;
    margin-top: 50px;
    border-color: red;
    position: relative;
  `;

  function ToggleDark(toggleTo) {
    if (toggleTo) {
      import('./files/dark').then(x => setDarkMode(x.default));
      localStorage.setItem("dark", "true");
    } else if (DarkMode !== toggleTo) {
      setDarkMode(toggleTo);
      localStorage.removeItem("dark");
    }
  }

  function ToggleDarkByMatchMedia(){
    const isDark = window.matchMedia('(prefers-color-scheme: dark)').matches;
    ToggleDark(isDark);
  }

  window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change',ToggleDarkByMatchMedia);

  if(!localStorage.getItem("had-math-dark")){
    useEffect(ToggleDarkByMatchMedia);
    localStorage.setItem("had-math-dark", true);
  }

  ToggleDark(DarkMode);

  return (
    <Router>
      <style>
        {DarkMode ? DarkMode : ''}
      </style>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            <img src="/logo.svg" height="30px" className="me-3 ms-2" />
            My folder online
          </Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#basic-navbar-nav" aria-controls="basic-navbar-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="basic-navbar-nav">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavMenu name="Home" link="" />
              <NavMenu name="Files" />
              {isLogin ? <li className="nav-item">
                <span className="nav-link" style={{ cursor: 'pointer' }} aria-current="page" onClick={LogoutNow}>Logout</span>
              </li> : null}
            </ul>
            <div className="form-check form-switch me-4">
              <input className="form-check-input" type="checkbox" checked={Boolean(DarkMode)} onChange={e => ToggleDark(e.target.checked)} id="SwitchDarkMode" />
              <label className={"form-check-label" + (DarkMode ? ' text-light' : '')} htmlFor="SwitchDarkMode">{DarkMode ? 'dark' : 'light'}</label>
            </div>
            <BuyMeACoffee/>
            <a className="nav-link" style={{ cursor: 'pointer' }} href="https://gitlab.com/ido.very.much/my-folder-online" target="_blank">
              <img src="/gitlab.svg" height="30px" />
            </a>
          </div>
        </div>
      </nav>
      <div className="content p-3">
        <Suspense fallback={<RingLoader size={200} css={override} color="rgb(54 148 215)" />}>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/Download" render={() => <Download DarkMode={DarkMode}/>} />
            <Route path="/FastLink/:id" component={FastLink} />
            <Route path="/Files" component={isLogin ? Files : Login} />
            <Route path="/Login" component={Login} />
          </Switch>
        </Suspense>
      </div>
    </Router>
  );
}

export default App;
