![My Logo](https://my-folder-online.vercel.app/favicon.ico)

# Share your local folder easily
Simple app, that helps you share your local folder for view only (with downloads and zips).

## Features!

  - You can download as zip
  - Have preview for
    - Images: png, jpeg, gif, bmp, including 360-degree images
    - pdf, docx, txt
    - csv, xslx
    - Video: mp4, webm
    - Audio: mp3, ogg, wav
    - md (Markdown)
    - HTML, JavasScript, CSS, TypeScript, Golang, C#, Java, etc...
  - Fast, mulitypart download

### Easy to use

All you need to do is to input a folder path and start the server.
Then you get a fast link that you can share.

[Try it now][1]

[1]: https://my-folder-online.vercel.app/
